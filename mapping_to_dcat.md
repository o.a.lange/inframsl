# Mapping labs.json to epos-dcat-ap

# Facility

- type -> dct:type (e.g. laboratory)
- lab_id -> dct:identifier
- research_infrastructure_name -> dct:isPartOf (Facility)
- facility_name -> dct:title (e.g. lab name)
- tna_service -> dct:relation -> rdfs:Resource -> epos::service -> TNA_Service
- data_service -> dct:relation -> rdfs:Resource ->  Data_Service
- address -> vcard:hasAddress
- website -> foaf:page -> foaf:Document
- gps -> will be added
 
# Equipment



# Service